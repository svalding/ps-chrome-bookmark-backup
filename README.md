# PS Chrome Bookmark Backup

This GUI utility backs up a user's Chrome Bookmarks file to a timestamped-named folder in their home directory.
This utility is meant to be used in a business setting, however it *may* work on a local computer with enough tweaking.

#Getting the Utility

You needn't download the entire project, unless you want to play with the Source Code. You will find the latest release available for download on the [Downloads](https://bitbucket.org/svalding/ps-chrome-bookmark-backup/downloads/) page.

#First Run

Upon opening the utility for the first time, a config file will be generated for you. To edit the defaults go to Edit > Preferences and make your corrections. Press Update Configuration to commit your changes to the config file.

Once you have commit your changes go to Edit > Refresh Configuration to load the new settings.

#Main Window Controls

**Current User**

Gets bookmars from $env:localappdata's chrome folder, and copies them to the home directory

**Other Profile**

Dynamic list of C:\users. Selecting this option will copy the bookmarks to the user's directory by finding their home directory on the file server first. Caveat: If Home folder name != username, this will fail.

**Backup Button**

Clicking this will start the backup. Should only take a second. 

#Import

There is an import feature. To access the feature, select File > Import. This allows you to take a backup, and restore it to a chrome instance. The options work the same as backup, with Current User or Other User.

#Roadmap

~~- Allow Overwrites (this is in the Preferences, but not coded into the utility yet.)~~ _Fixed in #1_

- Enable Compressing backup to zip file. (Item exists in preferences, but not coded yet.)
- Dynamically update Backup Button text to reflect Home Drive letter preference setting.
- Add support for Home Directory from Active Directory