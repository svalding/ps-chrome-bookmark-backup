﻿#--------------------------------------------
# Declare Global Variables and Functions here
#--------------------------------------------


#Sample function that provides the location of the script
function Get-ScriptDirectory
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($null -ne $hostinvocation)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}

#Sample variable that provides the location of the script
[string]$ScriptDirectory = Get-ScriptDirectory


function Update-Config
{
	
	If (!(Test-Path "C:\ProgramData\Chrome Backup Utility\"))
	{
		New-Item -ItemType Directory -Path "C:\ProgramData" -Name "Chrome Backup Utility"
	}
	
	$configuration = [pscustomobject]@{
		
		"Home Drive" = $cbDriveLetter.SelectedItem
		"Server"	 = $txtFileServer.Text
		"Root"  = $txtHomeDir.Text
		"Allow Overwrite" = If ($chkOverwrite.Checked) { $true } Else { $false }
		"Compress Backup" = If ($chkCompress.Checked) { $true } Else { $false }
		
	}
	
	Set-Content -Path "C:\ProgramData\Chrome Backup Utility\config.json" -Value $($configuration | ConvertTo-Json) -Force
}

function New-Config
{
	If (!(Test-Path "C:\ProgramData\Chrome Backup Utility\config.json"))
	{
		Show-Preferences_psf
		
	}
}

function Import-Config
{
	Try
	{
		$configFile = Get-Content "C:\ProgramData\Chrome Backup Utility\config.json" -Raw -ErrorAction Stop
		$config = $configFile | ConvertFrom-Json
		
		$global:driveLetter = $config."Home Drive"
		$global:fileServer = $config.Server
		$global:root = $config.Root
	}
	
	Catch
	{
		
		[System.Windows.Forms.Messagebox]::Show("An error occured importing your configuration. $($_.Exception.Message)", "Import Error", 'OK',"Error")
			
	}
	
	
}