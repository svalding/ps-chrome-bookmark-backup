﻿#--------------------------------------------
# Declare Global Variables and Functions here
#--------------------------------------------


#Sample function that provides the location of the script
function Get-ScriptDirectory
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($null -ne $hostinvocation)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}

#Sample variable that provides the location of the script
[string]$ScriptDirectory = Get-ScriptDirectory


function Update-Config
{
	
	If (!(Test-Path "$($env:LOCALAPPDATA)\Chrome Backup Utility\"))
	{
		New-Item -ItemType Directory -Path $env:LOCALAPPDATA -Name "Chrome Backup Utility"
	}
	
	$configuration = [pscustomobject]@{
		
		"Home Drive" = $cbDriveLetter.SelectedItem
		"Server"	 = $txtFileServer.Text
		"Root"	     = $txtHomeDir.Text
		"Nested" = If($chkNested.Checked) { $true } Else { $false } 
		"Allow Overwrite" = If ($chkOverwrite.Checked) { $true } Else { $false }
		"Compress Backup" = If ($chkCompress.Checked) { $true } Else { $false }
		
	}
	
	Set-Content -Path "$($env:LOCALAPPDATA)\Chrome Backup Utility\config.json" -Value $($configuration | ConvertTo-Json) -Force
}

function New-Config
{
	If (!(Test-Path "$($env:LOCALAPPDATA)\Chrome Backup Utility"))
	{
		New-Item -ItemType Directory -Path $env:LOCALAPPDATA -Name "Chrome Backup Utility"
	}

		$configuration = [pscustomobject]@{
			
			"Home Drive" = "H:\"
			"Server"	 = "fileserver.contoso.com"
			"Root"		      = "Home"
			"Nested" = "false"
			"Allow Overwrite" = "false"
			"Compress Backup" = "false"
			
		}
		
		New-Item -Path "$($env:LOCALAPPDATA)\Chrome Backup Utility" -Name config.json
		Set-Content -Path "$($env:LOCALAPPDATA)\Chrome Backup Utility\config.json" -Value $($configuration | ConvertTo-Json) -Force
		
	
}

function Import-Config
{
	Try
	{
		$configFile = Get-Content "$($env:LOCALAPPDATA)\Chrome Backup Utility\config.json" -ErrorAction Stop
		$config = $configFile | ConvertFrom-Json
		
		$global:driveLetter = $config."Home Drive"
		$global:fileServer = $config.Server
		$global:root = $config.Root
		$global:overwrite = $config."Allow Overwrite"
		$global:compress = $config. "Compress Backup"
		$global:nested = $config.Nested
		
	}
	
	Catch
	{
		
		[System.Windows.Forms.Messagebox]::Show("An error occured importing your configuration. $($_.Exception.Message)", "Import Error", 'OK',"Error")
			
	}
	
	
}